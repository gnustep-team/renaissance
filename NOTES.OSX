Note on porting to OSX:

#include
========

On GNUstep, we always use #include, and the headers try to #include
only the minimum number of headers needed to compile.  The actual
source code uses precompiled headers, so it always include everything
we may eventually need.

On OSX, Objective-C headers are not protected against multiple
inclusions.  This is a bug in the OSX headers.  Neverthless,
Foundation/Foundation.h and AppKit/AppKit.h can be #included on OSX
(because they just #import other headers).  That is actually
recommended, since those headers are precompiled.  So, on OSX we
always #include <Foundation/Foundation.h> and #include
<AppKit/AppKit.h>

The typical #include pattern in Renaissance headers is then

#ifndef GNUSTEP
# include <Foundation/Foundation.h>
# include <AppKit/AppKit.h>
#else
# include <Foundation/NSDebug.h>
# include <Foundation/NSString.h>
# include <AppKit/NSButton.h>
#endif

In source files instead we just include everything and turn
precompiled headers on.

GNUmakefiles vs ProjectBuilder
==============================

On GNUstep, we compile using GNUmakefiles.  On OSX, we compile using
GNUmakefiles again.  At the beginning, I was using ProjectBuilder to
build on OSX.  That soon turned out to be a loosing strategy, since
ProjectBuilder stores project information into horrible semi-binary
files which can't be easily edited unless you open them in
ProjectBuilder.  Keeping the Apple Mac OS X port working while working
on Renaissance under GNU/Linux would prove difficult, since
ProjectBuilder is not available on GNU/Linux, and so I couldn't update
the Apple OSX port from GNU/Linux, and the files to build under Apple
OSX would soon go out of sync.

In short, keeping two separate build systems is a losing strategy,
particularly when one of the two is a proprietary unportable build
system.

So, I ported gnustep-make on Apple Mac OS X, and use gnustep-make on
both systems to build, which works very well.  Please note that you
don't need gnustep-make to actually use Renaissance, only to compile
it.

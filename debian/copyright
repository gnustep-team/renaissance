Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Renaissance
Upstream-Contact: gnustep-dev@gnu.org
Source: http://www.gnustep.it/Renaissance
Comment: The library is licensed under LGPL-2.1+ but the license
         notices in the source files are not updated.

Files: *
Copyright: 2001-2008 Free Software Foundation, Inc.
License: LGPL-2.1+

Files: Tools/* Examples/*
Copyright: 2001-2008 Free Software Foundation, Inc.
License: GPL-2+

Files: Examples/Applications/Templates/*
Copyright: 2003-2007 Free Software Foundation, Inc.
License: public-domain
 This file is part of Renaissance's Template Standard Application.
 You can use it as a starting point for your own programs, no matter
 what their copyright is.  You can remove this notice and replace it
 with your own.  This file is in the public domain.

Files: Documentation/*
Copyright: 2001-2008 Free Software Foundation, Inc.
License: permissive
 Permission is granted to make and distribute verbatim copies of this
 manual provided the copyright notice and this permission notice are
 preserved on all copies.
 .
 Permission is granted to copy and distribute modified versions of
 this manual under the conditions for verbatim copying, provided that
 the entire resulting derived work is distributed under the terms of a
 permission notice identical to this one.  Permission is granted to
 copy and distribute translations of this manual into another
 language, under the above conditions for modified versions.

Files: debian/*
Copyright: 2004-2025 Debian GNUstep maintainers
License: LGPL-2.1+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
 USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License, can be found in /usr/share/common-licenses/LGPL-2.1.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 You should have received a copy of the GNU General Public License
 along with this program; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in /usr/share/common-licenses/GPL-2.
